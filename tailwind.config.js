module.exports = {
  theme: {
    extend: {
      colors: {
        text: '#f5f5f5',
        background: '#696969',
      },
    },
  },
  variants: {
    borderWidth: ['responsive', 'hover', 'focus'],
    opacity: ['responsive', 'hover', 'focus'],
  },
  plugins: [],
}
