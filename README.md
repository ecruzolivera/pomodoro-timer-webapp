# Pomodoro Timer Web App

Basic [Pomodoro Timer](https://en.wikipedia.org/wiki/Pomodoro_Technique)

It's a PWA with offline functionality

Made with:
- [Create React App](https://facebook.github.io/create-react-app/)
- [Tailwindcss](https://tailwindcss.com/)

How to local build and deploy:

```bash
npm install
npm run build
serve build
```
