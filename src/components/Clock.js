import React from 'react'
import PropTypes from 'prop-types'

const Clock = ({ seconds, css }) => {
  const current = new Date(0)
  current.setSeconds(seconds)
  return (
    <div
      className={`${css} inline text-5xl text-center border-4 border-solid rounded-lg p-2`}
    >
      <span>
        {current
          .getMinutes()
          .toString()
          .padStart(2, '0')}
      </span>
      :
      <span>
        {current
          .getSeconds()
          .toString()
          .padStart(2, '0')}
      </span>
    </div>
  )
}

Clock.propTypes = {
  seconds: PropTypes.number.isRequired,
  css: PropTypes.string.isRequired,
}
export default Clock
