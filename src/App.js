import React, { Component } from 'react'
import Timer from 'advanced-timer'
import { get, set } from 'idb-keyval'
import Clock from './components/Clock'
import tomato from './assets/tomato.png'

class App extends Component {
  DefaultSession = 50 //min
  DefaultBreak = 10 //min
  TimeMax = 59
  TimeMin = 1
  MssgBreak = 'Take a break'
  MssgWork = 'Work MF!'
  NotificationLength = 5000 // 5 segs

  constructor(props) {
    super(props)
    this.state = {
      currentTime: this.DefaultSession * 60, // in seconds
      breakLength: this.DefaultBreak, // in minutes
      sessionLength: this.DefaultSession, // in minutes
      isRunning: false,
      isOnBreak: false,
      timer: new Timer(1000).action(this.onTimerOverFlow),
    }
  }

  async componentDidMount() {
    try {
      let sessionLength = await get('sessionLength')
      let breakLength = await get('breakLength')
      if (sessionLength && breakLength) {
        this.setState({
          currentTime: sessionLength * 60, // in seconds
          breakLength: breakLength, // in minutes
          sessionLength: sessionLength, // in minutes
        })
      }
    } catch (error) {
      console.warn(error)
    }
    await this.requestDesktopNotificationPermission()
  }
  componentWillUnmount() {
    this.state.timer.destroy()
  }

  onBreakClickChange = async e => {
    let value = e.currentTarget.value
    value = value < this.TimeMin ? this.TimeMin : value
    value = value > this.TimeMax ? this.TimeMax : value

    this.setState({
      breakLength: value,
    })
    try {
      await set('breakLength', value)
    } catch (error) {
      console.warn(error)
    }
  }

  onSessionClickChange = async e => {
    let value = e.currentTarget.value
    value = value < this.TimeMin ? this.TimeMin : value
    value = value > this.TimeMax ? this.TimeMax : value
    this.setState({
      sessionLength: value,
    })
    try {
      await set('sessionLength', value)
    } catch (error) {
      console.warn(error)
    }
  }

  onPlayPause = () => {
    if (this.state.timer.status === 'stopped') {
      this.setState(prevState => ({
        currentTime: prevState.sessionLength * 60,
        isRunning: true,
      }))
      this.state.timer.start()
    } else if (this.state.timer.status === 'running') {
      this.setState(prevState => ({
        isRunning: false,
      }))
      this.state.timer.pause()
    } else if (this.state.timer.status === 'paused') {
      this.setState(prevState => ({
        isRunning: true,
      }))
      this.state.timer.resume()
    } else {
      throw new Error(`Timer state ${this.state.timer.status} not valid`)
    }
  }

  onReload = () => {
    this.setState(prevState => ({
      currentTime: prevState.sessionLength * 60,
      isRunning: true,
      isOnBreak: false,
    }))
    if (this.state.timer.status !== 'stopped') {
      this.state.timer.stop()
    }
    this.state.timer.start()
  }

  onTimerOverFlow = () => {
    if (this.state.currentTime !== 0) {
      this.setState(prevState => ({
        currentTime: prevState.currentTime - 1,
      }))
    } else if (this.state.currentTime === 0) {
      if (this.state.isOnBreak) {
        this.notify(this.MssgWork)
        this.setState({
          isOnBreak: false,
          currentTime: this.state.sessionLength * 60,
        })
      } else {
        this.notify(this.MssgBreak)
        this.setState({
          isOnBreak: true,
          currentTime: this.state.breakLength * 60,
        })
      }
    }
  }

  requestDesktopNotificationPermission = async () => {
    if (Notification.permission && Notification.permission !== 'granted') {
      const permission = await Notification.requestPermission()
      if (permission !== 'granted') {
        alert(
          'Dear user, without notifications this site will not work properly',
        )
      }
    }
  }

  notify = text => {
    if (Notification.permission && Notification.permission === 'granted') {
      const notification = new Notification('Pomodoro Timer', {
        icon: tomato,
        body: text,
        vibrate: [200, 100, 200],
        tag: 'Pomodoro',
      })
      notification.onClick = function() {
        window.parent.focus()
        window.focus() //just in case, older browsers
        this.close()
      }
      setTimeout(notification.close.bind(notification), this.NotificationLength)
    }
  }

  render() {
    return (
      <div className='flex flex-col items-center min-h-screen'>
        <header>
          <h1 className='text-6xl mt-10'>Pomodoro</h1>
        </header>
        <main className='flex-grow flex flex-col items-center'>
          <h2 className='text-3xl text-center font-bold'>
            {this.state.isRunning === false
              ? 'Paused'
              : this.state.isOnBreak
              ? this.MssgBreak
              : this.MssgWork}
          </h2>
          <Clock seconds={this.state.currentTime} css='my-10 mx-auto' />
          <div className='flex justify-center mb-10'>
            <button onClick={this.onPlayPause}>
              <i
                className={`mx-4 fas ${
                  this.state.isRunning ? 'fa-pause' : 'fa-play'
                } fa-4x`}
              />
            </button>
            <button onClick={this.onReload}>
              <i className='mx-4 fas fa-redo fa-4x' />
            </button>
          </div>
          <ul className='flex justify-between'>
            <li key='0' className='flex flex-col items-center mx-4'>
              <label htmlFor='break' className='text-lg'>
                Break
              </label>
              <input
                name='break'
                type='number'
                min='1'
                max='59'
                value={this.state.breakLength}
                onChange={this.onBreakClickChange}
                className='text-bold w-10 bg-background'
              />
            </li>
            <li key='1' className='flex flex-col items-center mx-4'>
              <label htmlFor='session' className='text-lg'>
                Session
              </label>
              <input
                name='session'
                type='number'
                min='1'
                max='59'
                value={this.state.sessionLength}
                onChange={this.onSessionClickChange}
                className='text-bold w-10 bg-background'
              />
            </li>
          </ul>
        </main>
        <footer className='text-center m-10'>
          <p>Developed by:</p>
          <p>
            <a href='https://ecruzolivera.tech'>Ernesto Cruz Olivera</a>
          </p>
        </footer>
      </div>
    )
  }
}

export default App
